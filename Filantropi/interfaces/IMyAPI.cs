using System.Threading.Tasks;
using Filantropi.Filantropi.models;
using Refit;
using System.Net.Http;
namespace Filantropi.Filantropi.interfaces
{
    public interface IMyAPI
    {
       [Post("api/user")]
        Task<PostContent> SubmitPost([Body] PostContent post);
    }
}