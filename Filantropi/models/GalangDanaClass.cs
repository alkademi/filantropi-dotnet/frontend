﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Filantropi.Filantropi.models
{
    public class GalangDanaClass
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public string user { get; set; }
        public string event_title { get; set; }
        public string category { get; set; }
        public int target_fund { get; set; }
        public DateTime deadline { get; set; }
        public string description { get; set; }
        public string image_url { get; set; }
        public int status { get; set; }
    }
}