﻿using System.Net;
using Android.Graphics;

namespace Filantropi.Filantropi
{
    public class UtilityTools
    {
        // CREDIT: arnfada
        // SRC: https://stackoverflow.com/questions/28368455/xamarin-visual-studio-gridview-with-image-from-url
        public static Bitmap GetBitmapFromUrl(string url)
        {
            using(WebClient webClient = new WebClient())
            {
                byte[] bytes = webClient.DownloadData(url);
                if(bytes != null && bytes.Length > 0)
                {
                    return BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                }
            }
            return null;
        }

    }
}