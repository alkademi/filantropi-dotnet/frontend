﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json.Serialization;
using Android.Content.Res;
using Google.Android.Material.TextField;
using Newtonsoft.Json.Linq;
using Xamarin.Essentials;

namespace Filantropi.Filantropi.ui.login
{
    [Activity(Label = "LoginActivity")]
    public class LoginActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_login);
            // Create your application here
            Button btnToRegister = FindViewById<Button>(Resource.Id.login_btntoregister);
            
            
            TextInputEditText emailInput = FindViewById<TextInputEditText>(Resource.Id.email);
            TextInputEditText passwordInput = FindViewById<TextInputEditText>(Resource.Id.password);

            
            
            var client = new HttpClient();
            client.BaseAddress = new Uri("http://10.0.2.2:5000");
            
            btnToRegister.Click += delegate
            {
                Intent intent = new Intent(this, typeof(register.RegisterActivity));
                StartActivity(intent);
            };
            
            Button btnRegister = FindViewById<Button>(Resource.Id.login_btnlogin);
            btnRegister.Click += async (sender, e) =>
            {
                
                string data = @"{""email"" : """ + emailInput.Text + @"""";
                string data2 = @", ""password"" : """ + passwordInput.Text + @"""}";
                string sent = data + data2;
                
                Console.WriteLine(sent);
                
                var content = new StringContent (sent, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync("/api/user/authenticate", content);
                Console.WriteLine(response);
                
                Console.WriteLine(response.StatusCode);
                var result = await response.Content.ReadAsStringAsync();
                Console.WriteLine(result);
                
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Intent intent = new Intent(this, typeof(dashboard_admin.DashboardAdminActivity));
                   
                   var jsonResult =  JObject.Parse(result);
                   var token = jsonResult["token"].ToString();
                    var id = jsonResult["id"].ToString();
                   // store value 
                   await SecureStorage.SetAsync("token", token);
                   await SecureStorage.SetAsync("userId", id);
                   var myValue = await SecureStorage.GetAsync("token");
                   Console.WriteLine("The token " + myValue);
                   
                   StartActivity(intent);
                }
                else
                {
                    Toast.MakeText(ApplicationContext, "Username atau Password Salah", ToastLength.Long).Show();
                }
            };
        }
    }
}