﻿using System;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidX.RecyclerView.Widget;

namespace Filantropi.Filantropi.ui.doa
{
    public class DoaViewHolder : RecyclerView.ViewHolder
    {
        public ImageView Img { get; private set; }
        public TextView Nama { get; private set; }
        public TextView Komentar { get; private set; }

        public DoaViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            Img = itemView.FindViewById<ImageView>(Resource.Id.doa_img);
            Nama = itemView.FindViewById<TextView>(Resource.Id.doa_tvnama);
            Komentar = itemView.FindViewById<TextView>(Resource.Id.doa_tvkomentar);

            itemView.Click += (sender, e) => listener(base.LayoutPosition);
        }
    }
}




