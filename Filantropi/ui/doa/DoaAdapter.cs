﻿using System;
using System.Net;
using Android.Graphics;
using Android.Views;
using AndroidX.RecyclerView.Widget;

namespace Filantropi.Filantropi.ui.doa
{
    public class ListDoaAdapter : RecyclerView.Adapter
    {
        public ListDoa mListDoa;
        public event EventHandler<int> ItemClick;

        public ListDoaAdapter(ListDoa ListDoa)
        {
            mListDoa = ListDoa;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context)
                .Inflate(Resource.Layout.item_doa, parent, false);
            DoaViewHolder vh = new DoaViewHolder(itemView, OnClick);
            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            DoaViewHolder vh = holder as DoaViewHolder;
            // vh.Img.SetImageBitmap(GetBitmapFromUrl(mListDoa[position].imagesrc));
            vh.Nama.Text = mListDoa[position].nama;
            vh.Komentar.Text = mListDoa[position].komentar;
        }

        public override int ItemCount
        {
            get { return mListDoa.NumDoa; }
        }

        void OnClick(int position)
        {
            if (ItemClick != null)
                ItemClick(this, position);
        }
        private Bitmap GetBitmapFromUrl(string url)
        {
            using (WebClient webClient = new WebClient())
            {
                byte[] bytes = webClient.DownloadData(url);
                if (bytes != null && bytes.Length > 0)
                {
                    return BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                }
            }
            return null;
        }
    }
}