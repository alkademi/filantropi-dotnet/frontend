﻿using System;

namespace Filantropi.Filantropi.ui.doa
{
    public class Doa
    {
        public string imagesrc;
        public string nama;
        public string komentar;

        public Doa(string imagesrc, string nama, string komentar)
        {
            this.imagesrc = imagesrc;
            this.nama = nama;
            this.komentar = komentar;
        }
    }

    public class ListDoa
    {
        private Doa[] data;

        public ListDoa(Doa[] data)
        {
            this.data = data;
        }

        public static ListDoa generateSample()
        {
            string url = "https://www.kindpng.com/picc/m/24-248325_profile-picture-circle-png-transparent-png.png";
            Doa[] data =
            {
                new Doa(url, "Hamba Allah", "Bismillah.. semoga berkah buat adik2 santri, serta berkah untuk saya dan keluarga, diberikan kesehatan dan dilancarkan rejekinya, Aamiin"),
                new Doa(url, "Hamba Allah", "Bismillah.. semoga berkah buat adik2 santri, serta berkah untuk saya dan keluarga, diberikan kesehatan dan dilancarkan rejekinya, Aamiin"),
                new Doa(url, "Hamba Allah", "Bismillah.. semoga berkah buat adik2 santri, serta berkah untuk saya dan keluarga, diberikan kesehatan dan dilancarkan rejekinya, Aamiin"),
                new Doa(url, "Hamba Allah", "Bismillah.. semoga berkah buat adik2 santri, serta berkah untuk saya dan keluarga, diberikan kesehatan dan dilancarkan rejekinya, Aamiin"),
                new Doa(url, "Hamba Allah", "Bismillah.. semoga berkah buat adik2 santri, serta berkah untuk saya dan keluarga, diberikan kesehatan dan dilancarkan rejekinya, Aamiin"),
                new Doa(url, "Hamba Allah", "Bismillah.. semoga berkah buat adik2 santri, serta berkah untuk saya dan keluarga, diberikan kesehatan dan dilancarkan rejekinya, Aamiin"),
                new Doa(url, "Hamba Allah", "Bismillah.. semoga berkah buat adik2 santri, serta berkah untuk saya dan keluarga, diberikan kesehatan dan dilancarkan rejekinya, Aamiin"),
                new Doa(url, "Hamba Allah", "Bismillah.. semoga berkah buat adik2 santri, serta berkah untuk saya dan keluarga, diberikan kesehatan dan dilancarkan rejekinya, Aamiin"),
                new Doa(url, "Hamba Allah", "Bismillah.. semoga berkah buat adik2 santri, serta berkah untuk saya dan keluarga, diberikan kesehatan dan dilancarkan rejekinya, Aamiin"),
                new Doa(url, "Hamba Allah", "Bismillah.. semoga berkah buat adik2 santri, serta berkah untuk saya dan keluarga, diberikan kesehatan dan dilancarkan rejekinya, Aamiin"),
                new Doa(url, "Hamba Allah", "Bismillah.. semoga berkah buat adik2 santri, serta berkah untuk saya dan keluarga, diberikan kesehatan dan dilancarkan rejekinya, Aamiin"),
                new Doa(url, "Hamba Allah", "Bismillah.. semoga berkah buat adik2 santri, serta berkah untuk saya dan keluarga, diberikan kesehatan dan dilancarkan rejekinya, Aamiin"),
                new Doa(url, "Hamba Allah", "Bismillah.. semoga berkah buat adik2 santri, serta berkah untuk saya dan keluarga, diberikan kesehatan dan dilancarkan rejekinya, Aamiin")
            };
            ListDoa temp = new ListDoa(data);
            return temp;
        }

        public Doa this[int index]
        {
            get => data[index];
            set => data[index] = value;
        }

        public int NumDoa
        {
            get
            {
                return this.data.Length;
            }
        }
    }
}