﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Widget;
using AndroidX.RecyclerView.Widget;
using Filantropi.Filantropi.models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Essentials;

namespace Filantropi.Filantropi.ui.daftar_galang_dana
{
    [Activity(Label = "ListGalangDanaActivity")]
    public class ListGalangDanaActivity : Activity
    {
        // Buttons
        Button openButton;
        Button processingButton;
        Button closedButton;
        ImageButton addButton;
        ImageButton helpButton;

        // RecyclerView
        RecyclerView mRecyclerView;
        RecyclerView.LayoutManager mLayoutManager;
        
        // Data
        ListGalangDana mListGalangDana;
        
        // Adapter
        ListGalangDanaAdapter mAdapter;

        protected override async void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);
            SetContentView(Resource.Layout.activity_daftar_galang_dana);
                        
            // Bind views
            openButton = FindViewById<Button>(Resource.Id.openButton);
            processingButton = FindViewById<Button>(Resource.Id.processingButton);
            closedButton = FindViewById<Button>(Resource.Id.closedButton);
            addButton = FindViewById<ImageButton>(Resource.Id.addButton);
            helpButton = FindViewById<ImageButton>(Resource.Id.helpButton);
            mRecyclerView = FindViewById<RecyclerView> (Resource.Id.recyclerView);
            
            // Initialize Buttons
            InitializeButtonOnclick();
            
            // Plug in the linear layout manager
            mLayoutManager = new GridLayoutManager (this, 2, GridLayoutManager.Vertical, false);
            mRecyclerView.SetLayoutManager (mLayoutManager);
            
            // Set default data list and list state
            // mListGalangDana = ListGalangDana.generateSample(3);

            // GET TOKEN
            var MyToken = await SecureStorage.GetAsync("token");
            var client = new HttpClient();
            client.BaseAddress = new Uri("http://10.0.2.2:5000");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", MyToken);


            // GET Galang Dana
            HttpResponseMessage response = await client.GetAsync("/api/galang_dana?status=Active");
            Console.WriteLine(response);
            var result = await response.Content.ReadAsStringAsync();
            Console.WriteLine(result);
            var galang = JsonConvert.DeserializeObject<List<GalangDanaClass>>(result);
            Console.WriteLine("Allahuma");

            Console.WriteLine(galang.Count.ToString());
            //var jsonResult = JObject.Parse(result);
            //Console.WriteLine(jsonResult);

            // create object to render
            GalangDana[] data = new GalangDana[galang.Count];
            for (int i = 0; i < galang.Count; i++)
            {
               
                data[i] = new GalangDana(
                     galang[i].id,
                    "https://picsum.photos/id/1/1280/720",
                   "Judul : " + galang[i].event_title,
                    galang[i].deadline,
                    0,
                    galang[i].target_fund,
                    "Deskripsi : " + galang[i].description);
            }
            ListGalangDana temp = new ListGalangDana(data);
            mListGalangDana = temp;

            // Set adapter
            SwapAdapter();
        }
        
        void OnItemClick (object sender, int position)
        {
            // Set onclick for items event here
            int itemNum = position + 1;
            Toast.MakeText(this, "This is item number " + itemNum + " with title " + mListGalangDana[position].judul, ToastLength.Short).Show();
        }
      
        

        void SwapAdapter()
        {
            mAdapter = new ListGalangDanaAdapter(mListGalangDana);
            mAdapter.ItemClick += OnItemClick;
            mRecyclerView.SetAdapter(mAdapter);
        }

        void InitializeButtonOnclick()
        {
            addButton.Click += delegate(object sender, EventArgs args)
            {
                Intent intent = new Intent(this, typeof(form_galang_dana.FormGalangDanaActivity));
                StartActivity(intent);
            };
            helpButton.Click += delegate(object sender, EventArgs args)
            {
                
            };
            openButton.Click += delegate(object sender, EventArgs args)
            {
                // Swap Button Color
                openButton.SetBackgroundResource(Resource.Color.colorGreySecondary);
                processingButton.SetBackgroundResource(Resource.Color.colorGreyPrimary);
                closedButton.SetBackgroundResource(Resource.Color.colorGreyPrimary);
                
                // Fetch list galang dana data
                mListGalangDana = ListGalangDana.generateSample(13);
                
                // Swap adapter
                SwapAdapter();
            };
            processingButton.Click += delegate(object sender, EventArgs args)
            {
                // Swap Button Color
                openButton.SetBackgroundResource(Resource.Color.colorGreyPrimary);
                processingButton.SetBackgroundResource(Resource.Color.colorGreySecondary);
                closedButton.SetBackgroundResource(Resource.Color.colorGreyPrimary);

                // Fetch list galang dana data
                mListGalangDana = ListGalangDana.generateSample(1);
                
                // Swap adapter
                SwapAdapter();
            };
            closedButton.Click += delegate(object sender, EventArgs args)
            {
                // Swap Button Color
                openButton.SetBackgroundResource(Resource.Color.colorGreyPrimary);
                processingButton.SetBackgroundResource(Resource.Color.colorGreyPrimary);
                closedButton.SetBackgroundResource(Resource.Color.colorGreySecondary);
                
                // Fetch list galang dana data
                mListGalangDana = ListGalangDana.generateSample(2);
                
                // Swap adapter
                SwapAdapter();
            };
        }
    }
}