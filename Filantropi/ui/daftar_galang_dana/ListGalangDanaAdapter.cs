﻿using System;
using System.Net;
using Android.Graphics;
using Android.Views;
using AndroidX.RecyclerView.Widget;

namespace Filantropi.Filantropi.ui.daftar_galang_dana
{
    public class ListGalangDanaAdapter : RecyclerView.Adapter
    {
        public ListGalangDana mListGalangDana;
        public event EventHandler<int> ItemClick;
        
        public ListGalangDanaAdapter(ListGalangDana listGalangDana)
        {
            mListGalangDana = listGalangDana;
        }
        
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context)
                .Inflate(Resource.Layout.donasi_card, parent, false);
            GalangDanaViewHolder vh = new GalangDanaViewHolder(itemView, OnClick);
            return vh;
        }
        
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            GalangDanaViewHolder vh = holder as GalangDanaViewHolder;
            vh.Image.SetImageBitmap(GetBitmapFromUrl(mListGalangDana[position].imagesrc)); 
            // URI(Uri.Parse(mListGalangDana[position].imagesrc));
            vh.Title.Text = mListGalangDana[position].judul;
            vh.Collected.Text = String.Format("Rp{0:C0}", mListGalangDana[position].terkumpul) ;
            vh.Deadline.Text = String.Format("{0:D} hari lagi", mListGalangDana[position].deadline.Day - DateTime.Now.Day);
            vh.Desc.Text = mListGalangDana[position].deskripsi;
            vh.PBar.Max = (int) mListGalangDana[position].target;
            vh.PBar.Progress = (int) mListGalangDana[position].terkumpul;
        }

        public override int ItemCount
        {
            get { return mListGalangDana.NumGalangDana; }
        }
        
        void OnClick (int position)
        {
            if (ItemClick != null)
                ItemClick (this, position);
        }
        
        // CREDIT: arnfada
        // SRC: https://stackoverflow.com/questions/28368455/xamarin-visual-studio-gridview-with-image-from-url
        private Bitmap GetBitmapFromUrl(string url)
        {
            using(WebClient webClient = new WebClient())
            {
                byte[] bytes = webClient.DownloadData(url);
                if(bytes != null && bytes.Length > 0)
                {
                    return BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                }
            }
            return null;
        }
    }
}