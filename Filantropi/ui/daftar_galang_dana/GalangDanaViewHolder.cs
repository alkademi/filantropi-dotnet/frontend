﻿using System;
using Android.Views;
using Android.Widget;
using AndroidX.RecyclerView.Widget;

namespace Filantropi.Filantropi.ui.daftar_galang_dana
{
    public class GalangDanaViewHolder : RecyclerView.ViewHolder
    {
        public ImageView Image { get; private set; }
        public TextView Title { get; private set; }
        public TextView Desc { get; private set; }
        public TextView Collected { get; private set; }
        public TextView Deadline { get; private set; }
        public ProgressBar PBar { get; private set; }
        
        public GalangDanaViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            Image = itemView.FindViewById<ImageView>(Resource.Id.imageView);
            Title = itemView.FindViewById<TextView>(Resource.Id.titleTextView);
            Desc = itemView.FindViewById<TextView>(Resource.Id.descTextView);
            Collected = itemView.FindViewById<TextView>(Resource.Id.collectedTextView);
            Deadline = itemView.FindViewById<TextView>(Resource.Id.deadlineTextView);
            PBar = itemView.FindViewById<ProgressBar>(Resource.Id.progressBar);

            itemView.Click += (sender, e) => listener(base.LayoutPosition);
        }
    }
}