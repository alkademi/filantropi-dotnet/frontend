﻿using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Xamarin.Essentials;

namespace Filantropi.Filantropi.ui.daftar_galang_dana
{
    public class GalangDana
    {
        public int id;
        public string imagesrc;
        public string judul;
        public DateTime deadline;
        public long terkumpul;
        public long target;
        public string deskripsi;

        public GalangDana(int id, string imagesrc, string judul, DateTime deadline,
        long terkumpul, long target, string deskripsi)
        {
            this.id = id;
            this.imagesrc = imagesrc;
            this.judul = judul;
            this.deadline = deadline;
            this.terkumpul = terkumpul;
            this.deskripsi = deskripsi;
            this.target = target;
        }
    }
    public class ListGalangDana
    {
        private GalangDana[] data;

        public ListGalangDana(GalangDana[] data)
        {
            this.data = data;
        }

        public static ListGalangDana generateSample(int n)
        {
            GalangDana[] data = new GalangDana[n];
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                int rand_int = rnd.Next(100000);
                long rand_trg = rnd.Next(10000000, 2000000000);
                long rand_cur = rnd.Next(0, (int)rand_trg);
                data[i] = new GalangDana(
                    rand_int,
                    "https://picsum.photos/id/1/1280/720",
                    String.Format("Judul-{0:D}", rand_int),
                    DateTime.Now,
                    rand_cur,
                    rand_trg,
                    String.Format("Deskripsi-{0:D}", rand_int));
            }
            ListGalangDana temp = new ListGalangDana(data);
            return temp;
        }



        public GalangDana this[int index]
        {
            get => data[index];
            set => data[index] = value;
        }

        public int NumGalangDana
        {
            get
            {
                return this.data.Length;
            }
        }
    }
}