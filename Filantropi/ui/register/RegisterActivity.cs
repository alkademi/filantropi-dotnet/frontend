﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Filantropi.Filantropi.interfaces;
using Filantropi.Filantropi.models;
using Google.Android.Material.TextField;
using Refit;
using System.Net.Http;
using Xamarin.Essentials;

namespace Filantropi.Filantropi.ui.register
{
    [Activity(Label = "RegisterActivity")]
    public class RegisterActivity : Activity
    {
   
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_register);
            // Create your application here

            TextInputEditText emailInput = FindViewById<TextInputEditText>(Resource.Id.email);
            TextInputEditText namaInput = FindViewById<TextInputEditText>(Resource.Id.nama);
            TextInputEditText passwordInput = FindViewById<TextInputEditText>(Resource.Id.password);
            
            
            
            Button btnToLogin = FindViewById<Button>(Resource.Id.register_btntologin);
            btnToLogin.Click += delegate
            {
                Intent intent = new Intent(this, typeof(login.LoginActivity));
                StartActivity(intent);
            };

            var client = new HttpClient();
            client.BaseAddress = new Uri("http://10.0.2.2:5000");
            
           // const string url = "https://10.0.2.2:5000/api/user";
          //  HttpClient _client = new HttpClient();
          //  var content = await _client.GetStringAsync(url); 
          //  Console.WriteLine("Gakuat ya Allah " + content);

            //IMyAPI myAPI = RestService.For<IMyAPI>("https://localhost:5000/");

            Button btnRegister = FindViewById<Button>(Resource.Id.register_btndaftar);
            btnRegister.Click +=  async (sender, e) => {
           
                // nambah data
                string data = @"{""name"" : """ + namaInput.Text + @"""";
                string data2 = @", ""password"" : """ + passwordInput.Text + @"""";
                string data3 = @", ""email"" : """ + emailInput.Text + @"""";
                string data4 = @", ""role"" : ""admin""}";
                string sent = data + data2 + data3 + data4;
                Console.WriteLine(sent);
                
                var content = new StringContent (sent, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync("/api/user", content);
                Console.WriteLine(response);

                // PostContent result = await myAPI.SubmitPost(register);
                 //Console.WriteLine(result);
                // Perform action on click
                // Console.WriteLine("Tolong dan Ampuni aku ya Allah");
                
                Console.WriteLine(response.StatusCode);
                var result = await response.Content.ReadAsStringAsync();
                Console.WriteLine(result);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Intent intent = new Intent(this, typeof(login.LoginActivity));
                    StartActivity(intent);
                  //  await Navigation.PushModalAsync(new MainPage());
                 //   var jsonResult =  JObject.Parse(result);
                 //   var token = jsonResult["token"].ToString();
                    // store value 
                 //   await SecureStorage.SetAsync("token", token);
                //    var myValue = await SecureStorage.GetAsync("token");
                 //   Console.WriteLine("The token " + myValue);
                }
              //  else
               // {
                 //   await DisplayAlert ("Alert", "Incorrect Credentials", "OK");
                
                //}
            };
        }
        
    }


}