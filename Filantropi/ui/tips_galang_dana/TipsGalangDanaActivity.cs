using Android.App;
using Android.OS;

namespace Filantropi.Filantropi.ui.tips_galang_dana
{
    [Activity(Label = "TipsGalangDanaActivity")]
    public class TipsGalangDanaActivity : Activity
    {
        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);
            SetContentView(Resource.Layout.activity_tips_galang_dana);
        }
    }
}