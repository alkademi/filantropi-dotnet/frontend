﻿using System;
using Android.Hardware.Camera2;

namespace Filantropi.Filantropi.ui.daftar_donasi
{
    public class Donasi
    {
        public string imagesrc;
        public string judul;
        public DateTime deadline;
        public long terkumpul;
        public long target;
        public string deskripsi;

        public Donasi(string imagesrc, string judul, DateTime deadline,
            long terkumpul, long target, string deskripsi)
        {
            this.imagesrc = imagesrc;
            this.judul = judul;
            this.deadline = deadline;
            this.terkumpul = terkumpul;
            this.deskripsi = deskripsi;
            this.target = target;
        }
    }
    
    public class ListDonasi
    {
        private Donasi[] data;

        public ListDonasi(Donasi[] data)
        {
            this.data = data;
        }

        public static ListDonasi generateSample()
        {
            Donasi[] data =
            {
                new Donasi("https://picsum.photos/id/1/1280/720", "Judul-1", DateTime.Now, 100, 200, "Desk-1"),
                new Donasi("https://picsum.photos/id/2/1280/720", "Judul-2", DateTime.Now, 100, 1000, "Desk-2"),
                new Donasi("https://picsum.photos/id/3/1280/720", "Judul-3", DateTime.Now, 234, 10000, "Desk-3"),
                new Donasi("https://picsum.photos/id/47/1280/720", "Judul-4", DateTime.Now, 4578, 10000, "Desk-4"),
                new Donasi("https://picsum.photos/id/57/1280/720", "Judul-5", DateTime.Now, 9999, 10000, "Desk-5"),
                new Donasi("https://picsum.photos/id/63/1280/720", "Judul-6", DateTime.Now, 5103113, 10000, "Desk-6"),
                new Donasi("https://picsum.photos/id/77/1280/720", "Judul-7", DateTime.Now, 1024, 10000, "Desk-7"),
                new Donasi("https://picsum.photos/id/81/1280/720", "Judul-8", DateTime.Now, 6318, 10000, "Desk-8"),
                new Donasi("https://picsum.photos/id/9/1280/720", "Judul-9", DateTime.Now, 222, 10000, "Desk-9"),
                new Donasi("https://picsum.photos/id/102/1280/720", "Judul-10", DateTime.Now, 30, 10000, "Desk-10"),
            };
            ListDonasi temp = new ListDonasi(data);
            return temp;
        }
        
        public Donasi this[int index]
        {
            get => data[index];
            set => data[index] = value;
        }

        public int NumDonasi
        {
            get
            {
                return this.data.Length;
            }
        }
    }
}