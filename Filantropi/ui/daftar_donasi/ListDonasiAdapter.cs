﻿using System;
using System.Net;
using Android.Graphics;
using Android.Views;
using AndroidX.RecyclerView.Widget;


namespace Filantropi.Filantropi.ui.daftar_donasi
{
    public class ListDonasiAdapter : RecyclerView.Adapter
    {
        public ListDonasi mListDonasi;
        public event EventHandler<int> ItemClick;
        
        public ListDonasiAdapter(ListDonasi listDonasi)
        {
            mListDonasi = listDonasi;
        }
        
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context)
                .Inflate(Resource.Layout.donasi_card, parent, false);
            DonasiViewHolder vh = new DonasiViewHolder(itemView, OnClick);
            return vh;
        }
        
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            DonasiViewHolder vh = holder as DonasiViewHolder;
            vh.Image.SetImageBitmap(GetBitmapFromUrl(mListDonasi[position].imagesrc)); // URI(Uri.Parse(mListDonasi[position].imagesrc));
            vh.Title.Text = mListDonasi[position].judul;
            vh.Collected.Text = String.Format("Rp{0:C0}", mListDonasi[position].terkumpul) ;
            vh.Deadline.Text = String.Format("{0:D} hari lagi", mListDonasi[position].deadline.Day - DateTime.Now.Day);
            vh.Desc.Text = mListDonasi[position].judul;
            vh.PBar.Max = (int) mListDonasi[position].target;
            vh.PBar.Progress = (int) mListDonasi[position].terkumpul;
        }

        public override int ItemCount
        {
            get { return mListDonasi.NumDonasi; }
        }
        
        void OnClick (int position)
        {
            if (ItemClick != null)
                ItemClick (this, position);
        }
        
        // CREDIT: arnfada
        // SRC: https://stackoverflow.com/questions/28368455/xamarin-visual-studio-gridview-with-image-from-url
        private Bitmap GetBitmapFromUrl(string url)
        {
            using(WebClient webClient = new WebClient())
            {
                byte[] bytes = webClient.DownloadData(url);
                if(bytes != null && bytes.Length > 0)
                {
                    return BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                }
            }
            return null;
        }
    }
}