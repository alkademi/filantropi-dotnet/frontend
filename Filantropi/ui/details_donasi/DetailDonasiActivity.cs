using Android.App;
using Android.OS;

namespace Filantropi.Filantropi.ui.details_donasi
{
    [Activity(Label = "DetailsDonasi")]
    public class DetailDonasiActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.activity_detailsdonasi);
        }
    }
}