﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Filantropi.Filantropi.ui.daftar_donasi;

namespace Filantropi.Filantropi.ui.temp
{
    [Activity(Label = "TempActivity", Theme = "@style/AppTheme", MainLauncher = true)]
    // login, register, dashadm, dompet, main
    public class TempActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_temp);

            Button login = FindViewById<Button>(Resource.Id.temp_login);
            login.Click += delegate
            {
                Intent intent = new Intent(this, typeof(login.LoginActivity));
                StartActivity(intent);
            };
            Button register = FindViewById<Button>(Resource.Id.temp_register);
            register.Click += delegate
            {
                Intent intent = new Intent(this, typeof(register.RegisterActivity));
                StartActivity(intent);
            };
            Button dashadm = FindViewById<Button>(Resource.Id.temp_dashadm);
            dashadm.Click += delegate
            {
                Intent intent = new Intent(this, typeof(dashboard_admin.DashboardAdminActivity));
                StartActivity(intent);
            };
            Button dompet = FindViewById<Button>(Resource.Id.temp_dompet);
            dompet.Click += delegate
            {
                Intent intent = new Intent(this, typeof(dompet.DompetActivity));
                StartActivity(intent);
            };
            Button main = FindViewById<Button>(Resource.Id.temp_main);
            main.Click += delegate
            {
                Intent intent = new Intent(this, typeof(MainActivity));
                StartActivity(intent);
            };
            Button form_galang_dana = FindViewById<Button>(Resource.Id.temp_form_galang_dana);
            form_galang_dana.Click += delegate
            {
                Intent intent = new Intent(this, typeof(form_galang_dana.FormGalangDanaActivity));
                StartActivity(intent);
            };
            Button daftar_donasi = FindViewById<Button>(Resource.Id.temp_daftar_donasi);
            daftar_donasi.Click += delegate
            {
                Intent intent = new Intent(this, typeof(daftar_donasi.ListDonasiActivity));
                StartActivity(intent);
            };
            Button melakukan_donasi = FindViewById<Button>(Resource.Id.temp_melakukan_donasi);
            melakukan_donasi.Click += delegate
            {
                Intent intent = new Intent(this, typeof(melakukan_donasi.MelakukanDonasiActivity));
                StartActivity(intent);
            };
            Button permintaan = FindViewById<Button>(Resource.Id.temp_permintaan);
            permintaan.Click += delegate
            {
                Intent intent = new Intent(this, typeof(permintaan_galang_dana.PermintaanActivity));
                StartActivity(intent);
            };
            Button doa = FindViewById<Button>(Resource.Id.temp_doa);
            doa.Click += delegate
            {
                Intent intent = new Intent(this, typeof(doa.DoaActivity));
                StartActivity(intent);
            };
            Button transaksi = FindViewById<Button>(Resource.Id.temp_transaksiadmin);
            transaksi.Click += delegate
            {
                Intent intent = new Intent(this, typeof(transaksi_admin.TransaksiAdminActivity));
                StartActivity(intent);
            };
            Button riwayat_donasi = FindViewById<Button>(Resource.Id.temp_riwayat_donasi);
            riwayat_donasi.Click += delegate
            {
                Intent intent = new Intent(this, typeof(RiwayatDonasiActivity));
                StartActivity(intent);
            };
            Button daftar_galang_dana = FindViewById<Button>(Resource.Id.temp_daftar_galang_dana);
            daftar_galang_dana.Click += delegate
            {
                Intent intent = new Intent(this, typeof(daftar_galang_dana.ListGalangDanaActivity));
                StartActivity(intent);
            };
            Button syarat_ketentuan = FindViewById<Button>(Resource.Id.temp_syarat_ketentuan);
            syarat_ketentuan.Click += delegate
            {
                Intent intent = new Intent(this, typeof(syarat_dan_ketentuan.SyaratDanKetentuanActivity));
                StartActivity(intent);
            };
            Button tentang_kami = FindViewById<Button>(Resource.Id.temp_tentang_kami);
            tentang_kami.Click += delegate
            {
                Intent intent = new Intent(this, typeof(tentang_kami.TentangKamiActivity));
                StartActivity(intent);
            };
            
            Button tips_galang_dana = FindViewById<Button>(Resource.Id.temp_tips);
            tips_galang_dana.Click += delegate
            {
                Intent intent = new Intent(this, typeof(tips_galang_dana.TipsGalangDanaActivity));
                StartActivity(intent);
            };
        }
    }
}