using Android.App;
using Android.OS;

namespace Filantropi.Filantropi.ui.syarat_dan_ketentuan
{
    [Activity(Label = "SyaratDanKetentuanActivity")]
    public class SyaratDanKetentuanActivity : Activity
    {
        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);
            SetContentView(Resource.Layout.activity_syarat_ketentuan);
        }
    }
}