﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Filantropi.Filantropi.ui.dompet
{
    [Activity(Label = "DompetActivity")]
    public class DompetActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.activity_dompet);
            RadioButton radtambah = FindViewById<RadioButton>(Resource.Id.dompet_radtambah);
            RadioButton radtarik = FindViewById<RadioButton>(Resource.Id.dompet_radtarik);
            // akses text = radtambah.Text

            Button kirim = FindViewById<Button>(Resource.Id.dompet_btnkirim);
            kirim.Click += (sender, e) =>
            {
                Intent intent = new Intent(this, typeof(details_donasi.DetailDonasiActivity));
                StartActivity(intent);
            };
        }
    }
}