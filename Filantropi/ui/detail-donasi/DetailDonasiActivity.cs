using Android.App;
using Android.OS;

namespace Filantropi.Filantropi.ui.detail_donasi
{
    [Activity(Label = "DetailDonasiActivity")]
    public class DetailDonasiActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            SetContentView(Resource.Layout.activity_detaildonasi);
        }
    }
}