﻿using Android.App;
using Android.OS;
using Android.Widget;
using AndroidX.RecyclerView.Widget;

namespace Filantropi.Filantropi.ui.permintaan_galang_dana
{
    [Activity(Label = "PermintaanActivity")]
    public class PermintaanActivity : Activity
    {
        RecyclerView mRecyclerView;
        RecyclerView.LayoutManager mLayoutManager;
        ListPermintaan mListPermintaan;
        ListPermintaanAdapter mAdapter;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.activity_permintaan);
            mListPermintaan = ListPermintaan.generateSample();

            mRecyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            // Plug in the linear layout manager:
            mLayoutManager = new GridLayoutManager(this, 1, GridLayoutManager.Vertical, false);
            mRecyclerView.SetLayoutManager(mLayoutManager);

            // Plug in my adapter:
            mAdapter = new ListPermintaanAdapter(mListPermintaan);
            mRecyclerView.SetAdapter(mAdapter);
            mAdapter.ItemClick += OnItemClick;
        }

        void OnItemClick(object sender, int position)
        {
            // Set onclick for items event here
            int itemNum = position + 1;
            Toast.MakeText(this, "This is item number " + itemNum + " with title " + mListPermintaan[position].judul, ToastLength.Short).Show();
        }
    }
}