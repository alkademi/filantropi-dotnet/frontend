﻿using System;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidX.RecyclerView.Widget;

namespace Filantropi.Filantropi.ui.permintaan_galang_dana
{
    public class PermintaanViewHolder : RecyclerView.ViewHolder
    {
        public TextView Judul { get; private set; }
        public TextView Akun { get; private set; }
        public TextView Target { get; private set; }
        public ImageButton Accept { get; private set; }
        public ImageButton Decline { get; private set; }

        public PermintaanViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            Judul = itemView.FindViewById<TextView>(Resource.Id.permintaan_tvjudul);
            Akun = itemView.FindViewById<TextView>(Resource.Id.permintaan_tvakun);
            Target = itemView.FindViewById<TextView>(Resource.Id.permintaan_tvtarget);
            Accept = itemView.FindViewById<ImageButton>(Resource.Id.permintaan_btnyes);
            Decline = itemView.FindViewById<ImageButton>(Resource.Id.permintaan_btnno);

            itemView.Click += (sender, e) => listener(base.LayoutPosition);
        }
    }
}




