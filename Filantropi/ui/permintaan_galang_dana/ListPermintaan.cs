﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Filantropi.Filantropi.ui.permintaan_galang_dana
{
    public class Permintaan
    {
        public string judul;
        public string akun;
        public long target;

        public Permintaan(string judul, string akun, long target)
        {
            this.judul = judul;
            this.target = target;
            this.akun = akun;
        }
    }

    public class ListPermintaan
    {
        private Permintaan[] data;

        public ListPermintaan(Permintaan[] data)
        {
            this.data = data;
        }

        public static ListPermintaan generateSample()
        {
            Permintaan[] data =
            {
                new Permintaan("Bantu Abdul untuk bersekolah lagi!", "Indonesia berbagi", 100000000),
                new Permintaan("Bantu Abdul untuk bersekolah lagi!", "Indonesia berbagi", 100000000),
                new Permintaan("Bantu Abdul untuk bersekolah lagi!", "Indonesia berbagi", 100000000),
                new Permintaan("Bantu Abdul untuk bersekolah lagi!", "Indonesia berbagi", 100000000),
                new Permintaan("Bantu Abdul untuk bersekolah lagi!", "Indonesia berbagi", 100000000),
                new Permintaan("Bantu Abdul untuk bersekolah lagi!", "Indonesia berbagi", 100000000),
                new Permintaan("Bantu Abdul untuk bersekolah lagi!", "Indonesia berbagi", 100000000),
                new Permintaan("Bantu Abdul untuk bersekolah lagi!", "Indonesia berbagi", 100000000),
                new Permintaan("Bantu Abdul untuk bersekolah lagi!", "Indonesia berbagi", 100000000),
                new Permintaan("Bantu Abdul untuk bersekolah lagi!", "Indonesia berbagi", 100000000),
                new Permintaan("Bantu Abdul untuk bersekolah lagi!", "Indonesia berbagi", 100000000),
                new Permintaan("Bantu Abdul untuk bersekolah lagi!", "Indonesia berbagi", 100000000)
            };
            ListPermintaan temp = new ListPermintaan(data);
            return temp;
        }

        public Permintaan this[int index]
        {
            get => data[index];
            set => data[index] = value;
        }

        public int NumPermintaan
        {
            get
            {
                return this.data.Length;
            }
        }
    }
}