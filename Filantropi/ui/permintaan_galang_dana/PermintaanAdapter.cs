﻿using System;
using System.Net;
using Android.Graphics;
using Android.Views;
using AndroidX.RecyclerView.Widget;

namespace Filantropi.Filantropi.ui.permintaan_galang_dana
{
    public class ListPermintaanAdapter : RecyclerView.Adapter
    {
        public ListPermintaan mListPermintaan;
        public event EventHandler<int> ItemClick;

        public ListPermintaanAdapter(ListPermintaan ListPermintaan)
        {
            mListPermintaan = ListPermintaan;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context)
                .Inflate(Resource.Layout.item_permintaan, parent, false);
            PermintaanViewHolder vh = new PermintaanViewHolder(itemView, OnClick);
            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            PermintaanViewHolder vh = holder as PermintaanViewHolder;
            vh.Judul.Text = mListPermintaan[position].judul;
            vh.Akun.Text = mListPermintaan[position].akun;
            vh.Target.Text = String.Format("Rp{0:C0}", mListPermintaan[position].target);
        }

        public override int ItemCount
        {
            get { return mListPermintaan.NumPermintaan; }
        }

        void OnClick(int position)
        {
            if (ItemClick != null)
                ItemClick(this, position);
        }
    }
}