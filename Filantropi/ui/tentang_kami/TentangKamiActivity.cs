using Android.App;
using Android.OS;

namespace Filantropi.Filantropi.ui.tentang_kami
{
    [Activity(Label = "TentangKamiActivity")]
    public class TentangKamiActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.activity_tentang_kami);
        }
    }
}