﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Xamarin.Essentials;

namespace Filantropi.Filantropi.ui.dashboard_admin
{
    [Activity(Label = "DashboardAdminActivity")]
    public class DashboardAdminActivity : Activity
    {
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_dashboardadmin);

            var MyToken = await SecureStorage.GetAsync("token");
            var MyId = await SecureStorage.GetAsync("userId");
            Console.WriteLine("My Id " + MyId);

            // GET TOKEN
            var client = new HttpClient();
            client.BaseAddress = new Uri("http://10.0.2.2:5000");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", MyToken);


            // GET User State
            HttpResponseMessage response = await client.GetAsync("/api/user/" + MyId );
            Console.WriteLine(response);
            var result = await response.Content.ReadAsStringAsync();
            var jsonResult = JObject.Parse(result);
            Console.WriteLine(jsonResult);

            TextView name = FindViewById<TextView>(Resource.Id.nameTextView);
            name.Text = jsonResult["name"].ToString();


            Button btnedit = FindViewById<Button>(Resource.Id.dashadm_btnedit);
            btnedit.Click += delegate
            {
                Intent intent = new Intent(this, typeof(temp.TempActivity));
                StartActivity(intent);

            };
            LinearLayout llpermintaan = FindViewById<LinearLayout>(Resource.Id.dashdm_llpermintaan);
            llpermintaan.Click += delegate
            {
                Intent intent = new Intent(this, typeof(temp.TempActivity));
                StartActivity(intent);
            };
            LinearLayout lldaftargalang = FindViewById<LinearLayout>(Resource.Id.dashadm_lldaftargalang);
            lldaftargalang.Click += delegate
            {
                Intent intent = new Intent(this, typeof(temp.TempActivity));
                StartActivity(intent);
            };
            LinearLayout lldaftaruser = FindViewById<LinearLayout>(Resource.Id.dashadm_lldaftaruser);
            lldaftaruser.Click += delegate
            {
                Intent intent = new Intent(this, typeof(temp.TempActivity));
                StartActivity(intent);
            };
            LinearLayout lldaftartransaksi = FindViewById<LinearLayout>(Resource.Id.dashadm_lldaftartransaksi);
            lldaftartransaksi.Click += delegate
            {
                Intent intent = new Intent(this, typeof(temp.TempActivity));
                StartActivity(intent);
            };
            LinearLayout llkeluar = FindViewById<LinearLayout>(Resource.Id.dashadm_llkeluar);
            llkeluar.Click += delegate
            {
                Intent intent = new Intent(this, typeof(temp.TempActivity));
                StartActivity(intent);
            };

        }

    }
}