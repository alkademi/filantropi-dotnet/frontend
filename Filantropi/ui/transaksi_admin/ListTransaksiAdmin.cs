﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Filantropi.Filantropi.ui.transaksi_admin
{
    public class Transaksi
    {
        public string status; // in atau out
        public string id;
        public string nama;
        public long nominal;
        public DateTime tanggal;

        public Transaksi(string status, string id, string nama, long nominal, DateTime tanggal)
        {
            this.status = status;
            this.id = id;
            this.nominal = nominal;
            this.nama = nama;
            this.tanggal = tanggal;
        }
    }

    public class ListTransaksi
    {
        private Transaksi[] data;

        public ListTransaksi(Transaksi[] data)
        {
            this.data = data;
        }

        public static ListTransaksi generateSample()
        {
            Transaksi[] data =
            {
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now),
                new Transaksi("IN", "ID13423", "Bambang", 100000, DateTime.Now)
            };
            ListTransaksi temp = new ListTransaksi(data);
            return temp;
        }

        public Transaksi this[int index]
        {
            get => data[index];
            set => data[index] = value;
        }

        public int NumTransaksi
        {
            get
            {
                return this.data.Length;
            }
        }
    }
}