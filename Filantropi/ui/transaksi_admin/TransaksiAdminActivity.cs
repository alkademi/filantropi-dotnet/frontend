﻿using Android.App;
using Android.OS;
using Android.Widget;
using AndroidX.RecyclerView.Widget;

namespace Filantropi.Filantropi.ui.transaksi_admin
{
    [Activity(Label = "TransaksiAdminActivity")]
    public class TransaksiAdminActivity : Activity
    {
        RecyclerView mRecyclerView;
        RecyclerView.LayoutManager mLayoutManager;
        ListTransaksi mListTransaksiAdmin;
        ListTransaksiAdminAdapter mAdapter;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.activity_transaksi_admin);
            mListTransaksiAdmin = ListTransaksi.generateSample();

            mRecyclerView = FindViewById<RecyclerView>(Resource.Id.rv_transaksiadmin);
            // Plug in the linear layout manager:
            mLayoutManager = new GridLayoutManager(this, 1, GridLayoutManager.Vertical, false);
            mRecyclerView.SetLayoutManager(mLayoutManager);

            // Plug in my adapter:
            mAdapter = new ListTransaksiAdminAdapter(mListTransaksiAdmin);
            mRecyclerView.SetAdapter(mAdapter);
            mAdapter.ItemClick += OnItemClick;
        }

        void OnItemClick(object sender, int position)
        {
            // Set onclick for items event here
            int itemNum = position + 1;
            Toast.MakeText(this, "This is item number " + itemNum + " with title " + mListTransaksiAdmin[position].id, ToastLength.Short).Show();
        }
    }
}