﻿using System;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidX.RecyclerView.Widget;

namespace Filantropi.Filantropi.ui.transaksi_admin
{
    public class TransaksiAdminViewHolder : RecyclerView.ViewHolder
    {
        public TextView Status { get; private set; }
        public TextView Id { get; private set; }
        public TextView Nominal { get; private set; }
        public TextView Nama { get; private set; }
        public TextView Tanggal { get; private set; }

        public TransaksiAdminViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            Status = itemView.FindViewById<TextView>(Resource.Id.transaksi_status);
            Id = itemView.FindViewById<TextView>(Resource.Id.transaksi_tvid);
            Nominal = itemView.FindViewById<TextView>(Resource.Id.transaksi_tvnominal);
            Nama = itemView.FindViewById<TextView>(Resource.Id.transaksi_tvnama);
            Tanggal = itemView.FindViewById<TextView>(Resource.Id.transaksi_tvtanggal);

            itemView.Click += (sender, e) => listener(base.LayoutPosition);
        }
    }
}




