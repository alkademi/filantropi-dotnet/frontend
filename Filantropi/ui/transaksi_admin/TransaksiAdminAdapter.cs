﻿using System;
using System.Net;
using Android.Graphics;
using Android.Views;
using AndroidX.RecyclerView.Widget;

namespace Filantropi.Filantropi.ui.transaksi_admin
{
    public class ListTransaksiAdminAdapter : RecyclerView.Adapter
    {
        public ListTransaksi mListTransaksiAdmin;
        public event EventHandler<int> ItemClick;

        public ListTransaksiAdminAdapter(ListTransaksi ListTransaksiAdmin)
        {
            mListTransaksiAdmin = ListTransaksiAdmin;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context)
                .Inflate(Resource.Layout.item_transaksi, parent, false);
            TransaksiAdminViewHolder vh = new TransaksiAdminViewHolder(itemView, OnClick);
            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            TransaksiAdminViewHolder vh = holder as TransaksiAdminViewHolder;
            vh.Status.Text = mListTransaksiAdmin[position].status;
            vh.Id.Text = mListTransaksiAdmin[position].id;
            vh.Nominal.Text = String.Format("Rp{0:C0}", mListTransaksiAdmin[position].nominal);
            vh.Nama.Text = mListTransaksiAdmin[position].nama;
            vh.Tanggal.Text = String.Format("{0:D}",mListTransaksiAdmin[position].tanggal);
        }
        

        public override int ItemCount
        {
            get { return mListTransaksiAdmin.NumTransaksi; }
        }

        void OnClick(int position)
        {
            if (ItemClick != null)
                ItemClick(this, position);
        }
    }
}