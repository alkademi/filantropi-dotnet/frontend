using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace Filantropi.Filantropi.ui.melakukan_donasi
{
    [Activity(Label = "MelakukanDonasiActivity")]
    public class MelakukanDonasiActivity : Activity
    {
        // Donasi Info
        private ImageView imageView;
        private TextView titleTextView;
        // Saldo
        private TextView balanceTextView;
        private Button addBalanceButton;
        // Jumlah Donasi
        private EditText amountEditText;
        // Doa
        private EditText prayEditText;
        // Button Donasi
        private Button donasiButton;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_melakukan_donasi);
            // View binding
            imageView = FindViewById<ImageView>(Resource.Id.imageView);
            titleTextView = FindViewById<TextView>(Resource.Id.judulTextView);
            balanceTextView = FindViewById<TextView>(Resource.Id.rpsaldoTextView);
            addBalanceButton = FindViewById<Button>(Resource.Id.tambahButton);
            amountEditText = FindViewById<EditText>(Resource.Id.jumlahEditText);
            prayEditText = FindViewById<EditText>(Resource.Id.doaEditText);
            donasiButton = FindViewById<Button>(Resource.Id.donasiButton);
            
            // Set saldo
            string temp_saldo = "0";
            balanceTextView.Text = temp_saldo;
            
            // Set judul
            string temp_title = "Judul Sementara";
            titleTextView.Text = temp_title;
            
            // Set image
            string temp_url = "https://picsum.photos/id/237/200/300";
            imageView.SetImageBitmap(UtilityTools.GetBitmapFromUrl(temp_url));
            
            // Add button onclick listener
            addBalanceButton.Click += delegate(object sender, EventArgs args)
            {
                Intent intent = new Intent(this, typeof(dompet.DompetActivity));
                StartActivity(intent);
            };
            
            donasiButton.Click += delegate(object sender, EventArgs args)
            {
                if (amountEditText.Text == "" || amountEditText.Text == null)
                {
                    Toast.MakeText(this, "Masukkan jumlah donasi", ToastLength.Long).Show();
                    return;
                }
                else if (long.Parse(amountEditText.Text) <= 0)
                {
                    Toast.MakeText(this, "Jumlah donasi harus lebih dari Rp0", ToastLength.Long).Show();
                    return;
                }
                else if (long.Parse(balanceTextView.Text) < long.Parse(amountEditText.Text))
                {
                    Toast.MakeText(this, "Saldo tidak mencukupi", ToastLength.Long).Show();
                    return;
                }
                // Implement donasi request here
                Toast.MakeText(this, "Berhasil melakukan donasi!", ToastLength.Long).Show();
            };
        }
    }
}