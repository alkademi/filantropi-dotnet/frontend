using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Widget;
using Xamarin.Essentials;
using static Filantropi.Filantropi.UtilityTools;

namespace Filantropi.Filantropi.ui.form_galang_dana
{
    [Activity(Label = "FormGalangDana")]
    public class FormGalangDanaActivity : Activity
    {
        private EditText titleEditText;
        private EditText targetEditText;
        private EditText deadlineEditText;
        private EditText descEditText;
        private EditText imgsrcEditText;
        private ImageView imgView;
        private Button imgPreviewButton;
        private Button submitButton;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_form_galang_dana);
            // View binding
            titleEditText = FindViewById<EditText>(Resource.Id.titleEditText);
            targetEditText = FindViewById<EditText>(Resource.Id.targetEditText);
            deadlineEditText = FindViewById<EditText>(Resource.Id.deadlineEditText);
            descEditText = FindViewById<EditText>(Resource.Id.descEditText);
            imgsrcEditText = FindViewById<EditText>(Resource.Id.imgsrcEditText);
            imgView = FindViewById<ImageView>(Resource.Id.imageView);
            imgPreviewButton = FindViewById<Button>(Resource.Id.imgPreviewButton);
            submitButton = FindViewById<Button>(Resource.Id.submitButton);

            var MyToken = await SecureStorage.GetAsync("token");
            // GET TOKEN
            var client = new HttpClient();
            client.BaseAddress = new Uri("http://10.0.2.2:5000");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", MyToken);

            // Add onclick event
            imgPreviewButton.Click += delegate(object sender, EventArgs args)
            {
                if (imgsrcEditText.Text != null)
                {
                    Bitmap bm;
                    try
                    {
                        bm = UtilityTools.GetBitmapFromUrl(imgsrcEditText.Text);
                        if (bm != null)
                        {
                            imgView.SetImageBitmap(UtilityTools.GetBitmapFromUrl(imgsrcEditText.Text));
                        }
                        else
                        {
                            Toast.MakeText(this, "Unable to load image", ToastLength.Long).Show();
                        }
                    }
                    catch (Exception)
                    {
                        Toast.MakeText(this, "Bad Image URL", ToastLength.Long).Show();
                    }
                }
            };
            
            submitButton.Click += async delegate (object sender, EventArgs args)
            {
                // Add button logic
                // POST SCHEMA
                // {
                //     "id": 0,
                //     "eventTitle": "string",
                //     "category": "string",
                //     "targetFund": 0,
                //     "deadline": "2022-03-17T05:14:11.416Z",
                //     "description": "string",
                //     "imageUrl": "string"
                // }

                var MyId = await SecureStorage.GetAsync("userId");
                Random rnd = new Random();
                DateTime time = DateTime.ParseExact("28.9.2015 05:50:00", "dd.M.yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                Console.WriteLine("wiu" + time);
                string data = @"{""event_title"" : """ + titleEditText.Text + @"""";
                string data2 = @", ""target_fund"" : """ + targetEditText.Text + @"""";
                string data3 = @", ""deadline"" : """ + "2022-10-11T17:00:00Z" + @"""";
                string data4 = @", ""description"" : """ + descEditText.Text + @"""";
                string data5 = @", ""image_url"" : """ + imgsrcEditText.Text + @"""";
                string data6 = @", ""user_id"" : """ + MyId + @"""";
                string data7 = @", ""id"" : """ + rnd.Next(100000) + @"""";
                string data8 = @", ""category"" : ""pendidikan""}";
                string sent = data + data2 + data3 + data4 + data5 + data6 + data7 +data8;
                Console.WriteLine(sent);

                var content = new StringContent(sent, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync("/api/galang_dana", content);
                Console.WriteLine(response);
                Console.WriteLine(response.StatusCode);
                var result = await response.Content.ReadAsStringAsync();
                Console.WriteLine(result);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Intent intent = new Intent(this, typeof(login.LoginActivity));
                    StartActivity(intent);
                }
                };
        }
        
    }
}