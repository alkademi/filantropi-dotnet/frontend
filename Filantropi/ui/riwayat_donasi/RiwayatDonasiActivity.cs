using Android.App;
using Android.OS;
using Android.Widget;
using AndroidX.RecyclerView.Widget;

namespace Filantropi.Filantropi.ui.daftar_donasi
{
    [Activity(Label = "RiwayatDonasiActivity")]
    public class RiwayatDonasiActivity : Activity
    {
        RecyclerView mRecyclerView;
        RecyclerView.LayoutManager mLayoutManager;
        ListDonasi mListDonasi;
        ListDonasiAdapter mAdapter;

        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);
            SetContentView(Resource.Layout.activity_riwayat_donasi);
            mListDonasi = ListDonasi.generateSample();
            
            mRecyclerView = FindViewById<RecyclerView> (Resource.Id.recyclerView);
            // Plug in the linear layout manager:
            mLayoutManager = new GridLayoutManager (this, 2, GridLayoutManager.Vertical, false);
            mRecyclerView.SetLayoutManager (mLayoutManager);

            // Plug in my adapter:
            mAdapter = new ListDonasiAdapter(mListDonasi);
            mRecyclerView.SetAdapter(mAdapter);
            mAdapter.ItemClick += OnItemClick;
        }
        
        void OnItemClick (object sender, int position)
        {
            // Set onclick for items event here
            int itemNum = position + 1;
            Toast.MakeText(this, "This is item number " + itemNum + " with title " + mListDonasi[position].judul, ToastLength.Short).Show();
        }
    }
}